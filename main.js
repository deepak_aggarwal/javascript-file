dojo.require("dijit.Dialog");
dojo.require("dojo.on");
dojo.ready(
    function(){
        //Global Variable //
        dojo.require("module.Ajax");
        var url; 
        var rest_store;   // JSON_RESTStore
        var memory_store;  // Offline memory store
        var store;         // Abstract store for both stores(above)
        ////////////////////
        
        // Register Class
        dojo.declare("Register",null,{
            event_nodes: null,
            constructor: function()
            {
                event_nodes= new Array();
            },
            register_node: function(url,node_id,event_type,type,handler)
            {
                switch(type)
                {
                    case "dialog_form" :
                        var target_node;
                        var temp_handle;   // for capturing dojo.connect submit instance
                        event_nodes[node_id]= new dijit.Dialog({
                            href:url,
                            onCancel: function(){
                                dojo.disconnect(temp_handle); // very very important. Used for removing duplicate event listener
                                dojo.on.once(dojo.byId(node_id),event_type,dialog_main_handler);  // Registering event again to button
                            },
                            onHide: function()
                            {
                                dojo.disconnect(temp_handle); // Important In order to remove old event Listener. Somehow previous 
                            //connect handler function are automatically called without any reason.
                            // Might be a bug in Dojo.
                            }
                            
                        });
                        
                        // responsible for setting up all things and submitting form through ajax 
                        function dialog_main_handler(){
                            event_nodes[node_id].show();  // showing dialog
                            temp_handle=event_nodes[node_id].connect("onsubmit", function (event){  // Listeninh for submit event on dialog
                                target_node= event.target; // retriving actual event
                                dojo.stopEvent(event);
                                dojo.xhrPost({
                                    form : target_node,
                                    handleAs: "text",
                                    load: function(data)
                                    {
                                        handler(data,event_nodes[node_id]);
                                    }
                                });   //dojo.xhrPost ends
                            }); // dojo.connect ends
                        }
                        dojo.on.once(dojo.byId(node_id),event_type,dialog_main_handler); 
                        break;
                        
                    case "dialog":
                        event_nodes[node_id]= new dijit.Dialog({
                            href:url     
                        });
                        
                }
            }
        });

       

        register_instance=new Register();
       
        function temp1(data,instance)
        {
            alert(data);
        }
        function get_team_settings()
        {
            dojo.removeClass("general_settings","active");
            dojo.addClass("team_settings","active");
            var temp = new Ajax();
            temp.initialize("raw_data", "null", url+"get_user/", function(data){
                dojo.byId("setting_content").innerHTML=data;
            });
            temp.send_data("get");
            
            // Prefetching existing members data 
            var members=store.get("type/user");
            // Adding dialog to add member button 
            temp.add_function(
                function(){
                    register_instance.register_node("admin/load_add_member_form/","add_member","click","dialog_form",temp1);
                    
                    //===========================================================================
                    // Loading existing users in offline data store    
                    dojo.when(members,function(result){
                
                        // function for adding nodes to dom structure 
                        function addRow(newObject){
                            var newNode = "<tr><td>"+(dojo.query("tbody tr").length+1)+"</td>"+"<td>"+newObject.firstName+" "+newObject.lastName+"</td>";
                            for(x in newObject){
                                if(x=="firstName"||x=="lastName"||x=="phone"){
                                    continue; 
                                }
                                newNode += "<td>"+newObject[x]+"</td>";
                            }
                            newNode+="</tr>";
                            require(["dojo/dom-construct","dojo/dom"],function(domConstruct,dom){
                                domConstruct.place(domConstruct.toDom(newNode),dom.byId("tbody"));
                            });
                        }
                
                
                        dojo.fromJson(result).forEach(function(res){
                            addRow(res);
                        });
                    }); 
                });
                
             
        //store.put({id:2,data:"hi"});
            
        }
        function get_general_settings()
        {
            dojo.removeClass("team_settings","active");
            dojo.addClass("general_settings","active");
            var temp = new Ajax();
            temp.initialize("raw_data", "null", url+"get_general_setting_plane/", function(data){
                dojo.byId("setting_content").innerHTML=data;
            });
            temp.send_data("get");
            temp.add_function(
                function()
                {
                    dojo.query("#add_profile").connect("onclick",function(event)
                    {
                        var twAuthWindow = window.open("/admin/add_twitter_account/", "TwitterAuthorizeWindow", 
                            "toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=no, menubar=no, resizable=no, width=700, height=500");
                    }); // dojo.query ends
                
                });
        }
        function get_settings_plane(data)    //Add both general settings and team setttings event listener.
        {
            dojo.query("#content_plane").orphan();
            div.innerHTML= data;
            document.body.appendChild(div);
            var general_settings=dojo.query("#general_settings");
            general_settings.connect("onclick",function(event){
                if(!dojo.hasClass("general_settings","active"))
                {
                    get_general_settings();
                }
            });
            
            var team_settings=dojo.query("#team_settings");
            team_settings.connect("onclick",function(event){
                if(!dojo.hasClass("team_settings","active"))
                {
                    get_team_settings();
                }
            });
        }
        function login_handler(data,instance){
            recieved_data= dojo.fromJson(data);
            if(recieved_data.login=="true")  //If user is a genuine one 
            {
                var temp = document.location.href;
                url= temp.slice(0,temp.length-2)+"/"+recieved_data.user_type + "/";
                var fetch = new Ajax();
                fetch.initialize("raw_data",null,url, function (data){
                    instance.hide();
                    div = document.createElement("div");
                    div.innerHTML= data;
                    document.body.appendChild(div);
                    dojo.create("a",{
                        innerHTML: "<i class=\"icon-user icon-white\"></i> "+recieved_data.username,
                        href:"#",
                        id:"account"
                    },dojo.byId("login_button"),"replace");
                    dojo.create("li",{
                        innerHTML:"<a href=\"#\" id= \"settings\"> <i class=\"icon-cog icon-white\"></i> Settings </a>"
                    },dojo.byId("account").parentNode,"after");
                    
                    dojo.query("#settings").connect("onclick",function(event){ 
                        var temp = new Ajax();
                        temp.initialize("raw_data",null,url+"settings/get_content_plane/",get_settings_plane);  //get main setting plane 
                        temp.send_data("get");
                    });
                });// fetch initialize ends 
                fetch.send_data("get");
                
                // Adding dojo store for offline cache as well as JsonREST store 
                require(["dojo/store/JsonRest","dojo/store/Memory","dojo/store/Cache"], function(JsonRestStore,MemoryStore,Cache){
                    rest_store = new JsonRestStore({
                        target: "/CodeIgniter_2.1.0/admin/rest/main/"
                    });
                    memory_store =  new MemoryStore();
                    store = new Cache(rest_store,memory_store);
                });

                // Instance for connecting to node.js server for notification service
                var socket= io.connect("http://localhost:8080",{
                    'reconnect': true,
                    'reconnection delay': 500
                });
                socket.on("connect",function(){
                    console.log("connected to server");
                });
            }
            else
            {
                alert("Wrong credentials");
            }
        }
        register_instance.register_node("login","login_button","click","dialog_form",login_handler);
    });// dojo.ready ends
