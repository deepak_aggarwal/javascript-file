dojo.provide("module.Ajax");
dojo.declare(
    "Ajax",
    null,
    {
        _data_to_send:null,    							// Attributes private Attributes
        _response_data :null,   							// Variable which recieve data 
        _var:null,						
        deferred_instance:null,
        constructor : function() {},
        _initialize_form: function(node)
        {
            _var={
                form: node,
                handleAs:"text",
                load: function(data){
                    this._response_data=data;
                }
            };  
        },
        initialize: function(type,node,url,handler)
        {
            switch(type)
            {
                case "form" :
                    _initialize_form(node);
                    break;
                case "raw_data" :
                    _var={
                        url: url,
                        handleAs:"text",
                        load: handler
                    };  
            }
        },
	
        send_data: function(method)
        {
            switch(method)
            {
                case "post":
                    deferred_instance=dojo.xhrPost(_var);
                    break;
                case "get" :
                    deferred_instance=dojo.xhrGet(_var);
                    break;
            }
        },
    
        get_data : function ()
        {
            return this._response_data;
        },
        
        add_function : function(function_name)
        {
            deferred_instance.addCallback(function_name);
        }
    });


